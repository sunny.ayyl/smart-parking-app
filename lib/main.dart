import 'dart:async';
import 'dart:convert';
import 'dart:io';
//import 'package:google_maps_flutter/google_maps_flutter.dart';
//import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/widgets.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:location/location.dart';
import 'package:bubble/bubble.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:ui';
import 'package:flutter_nfc_reader/flutter_nfc_reader.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyApp createState() => _MyApp();
}

final String address = "http://35.238.234.50"; //Normal
//final String address="10.0.2.2" //Android Vitural Device

Future<List<JSON_api>> API(http.Client client) async {
  final response = await client.get(
    "http://35.238.234.50/get_car_info/",
  );
  if (response.statusCode == 200) {
    is_sent = "submited";
  } else {
    is_sent = "Unexpected error";
    //var message = [];
    //print(is_sent);
  }

  //print(response.body);
  return parseList(response.body.toString().replaceAll(
      "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 3.2 Final//EN\">", ""));
}

Future<List> API_(http.Client client) async {
  final response = await client.get(
    "http://35.238.234.50/get_car_info/",
  );
  if (response.statusCode == 200) {
    is_sent = "submited";
  } else {
    is_sent = "Unexpected error";
    //var message = [];
    //print(is_sent);
  }

  //print(response.body);
  return json.decode(response.body);
}

List<JSON_api> parseList(String responseBody) {
  final parsed = jsonDecode(responseBody) as List;
  //print(parsed);
  final to_return = parsed.map((json) => JSON_api.fromJson(json)).toList();
  //print(to_return);
  return to_return;
}

/*
class JSON_api {
  final String license_plate;
  final String status;
  final String last_update;
  JSON_api({this.license_plate, this.status, this.last_update});
  factory JSON_api.fromJson(Map<String, dynamic> json) {
    return JSON_api(
        license_plate: json["car_license"] as String,
        status: json["status"] as String,
        last_update: json["last_update"] as String);
  }
}
*/
class JSON_api {
  String car_license;
  String status;
  String last_update;

  JSON_api(
      {required this.car_license,
      required this.status,
      required this.last_update});

  JSON_api.fromJson(Map<String, dynamic> json)
      : car_license = json["car_license"],
        status = json["status"],
        last_update = json["last_update"];

  Map<String, dynamic> toJson() => {
        "car_license": car_license,
        "status": status,
        "last_update": last_update
      };
  @override
  String toString() {
    return '[${this.car_license},${this.status},${this.last_update}]';
  }
}

var is_sent = "You haven't submit yet";

Future Chatbot(http.Client client, msg) async {
  final reply = await client.get(address + "/chatbot/" + msg);
  if (reply.headers != 200) {
    var message = [];
  }
  return await reply;
}

enum EnterOrExit { Enter, Exit }
EnterOrExit _enter_or_exit = EnterOrExit.Enter;
TextEditingController car_license_input = new TextEditingController();
TextEditingController message_to_sent = new TextEditingController();
TextEditingController car_textbox = new TextEditingController();

class _MyApp extends State<MyApp> {
  @override
  int index_now = 0;
  var title = "Smart Parking";
  var screen = "0";
  var respone_text = "";
  var image = Image.asset(
    'assets/image/parking.png',
  );
  late BitmapDescriptor parking_icon;
  void add_marker_() async {
    BitmapDescriptor.fromAssetImage(ImageConfiguration(size: Size(36, 36)),
            'assets/image/parking_icon.png')
        .then((d) {
      parking_icon = d;
    });
  }

  void initState() {
    super.initState();
    _islogin();
    add_marker_();
  }

  _islogin() async {
    SharedPreferences data = await SharedPreferences.getInstance();
    if (data.getBool("login") != true) {
      screen = "Register car";
      setState(() {});
    } else {
      screen = "0";
      setState(() {});
    }
  }

  save_car(car) async {
    SharedPreferences data = await SharedPreferences.getInstance();
    data.setString("car", car);
    data.setBool("login", true);
  }

  Future<String?> get_car(context) async {
    SharedPreferences data = await SharedPreferences.getInstance();
    if (data.containsKey("car") != true) {
      showDialog(
          context: context,
          builder: (contaxt) {
            return AlertDialog(
                title: Text("Fail"),
                actions: [
                  TextButton(
                      onPressed: () => Navigator.of(context).pop(),
                      child: Text("Ok")),
                  TextButton(
                      onPressed: () {
                        screen = "Register car";
                        setState(() {});
                        Navigator.of(context).pop();
                      },
                      child: Text("Register now"))
                ],
                content: StatefulBuilder(
                  builder: (context, setstate) {
                    return SingleChildScrollView(
                      child: ListBody(
                        children: [
                          Text("Fail to submit"),
                          Text("Try to register octopus car"),
                          Builder(builder: (context) {
                            if (pressed == true) {
                              return Text("Error: Invalid argument(s)");
                            } else {
                              return OutlineButton(
                                onPressed: () {
                                  setstate(() {
                                    pressed = true;
                                  });
                                },
                                child: Text("Show more information"),
                              );
                            }
                          })
                        ],
                      ),
                    );
                  },
                ));
          });
    } else {
      var car_data_ = await data.getString("car");
      return car_data_;
    }
  }

  final data = SharedPreferences.getInstance();
  bool redirected = true;
  bool show_button = true;
  DateTime selectedDate = DateTime.now().toLocal();
  TimeOfDay seletedTime = TimeOfDay.now();
  Completer<GoogleMapController> _controller = Completer();
  List<Widget> message = [];
  bool pressed = false;
  Widget build(BuildContext context) {
    void nfc(context) {
      String dialog_title = "Detecting octopus card";
      List<Widget> body_nfc_dialog = [
        Text(
            "Please put your octoupus card to NFC antenna area (usually on the back of mobile phone)"),
        SizedBox(
          height: 10,
        ),
        Transform.scale(
          scale: 1.5,
          child: Icon(Icons.nfc),
        )
      ];
      showDialog(
          context: context,
          builder: (context) {
            return StatefulBuilder(builder: (context, setstate) {
              FlutterNfcReader.read().then((response) {
                dialog_title =
                    "Detected octopus card (Detected but no any information,so it is future work)";
                body_nfc_dialog = [
                  Text("Detected octopus car"),
                  SizedBox(
                    height: 10,
                  ),
                  Transform.scale(
                    scale: 1.5,
                    child: Icon(Icons.nfc),
                  )
                ];
                setstate(() {});
              });
              return AlertDialog(
                content: SingleChildScrollView(
                  child: ListBody(
                    children: body_nfc_dialog,
                  ),
                ),
                title: Text(dialog_title),
              );
            });
          });
    }

    Future Sent_data(
        http.Client client, car_license, status, last_update, context) async {
      pressed = false;
      try {
        final response = await client.get(address +
            "/register_car/" +
            car_license +
            "/" +
            status +
            "/" +
            last_update);
      } on SocketException catch (e) {
        showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
                title: Text("Fail"),
                actions: [
                  TextButton(
                      onPressed: () => Navigator.of(context).pop(),
                      child: Text("Ok"))
                ],
                content: StatefulBuilder(
                  builder: (context, setstate) {
                    return SingleChildScrollView(
                      child: ListBody(
                        children: [
                          Text("Fail to submit"),
                          Text("Try to connect to nework"),
                          Builder(builder: (context) {
                            if (pressed == true) {
                              return Text("Error: SocketException");
                            } else {
                              return OutlineButton(
                                onPressed: () {
                                  setstate(() {
                                    pressed = true;
                                  });
                                },
                                child: Text("Show more information"),
                              );
                            }
                          })
                        ],
                      ),
                    );
                  },
                ));
          },
        );
      }
      final response = await client.get(address +
          "/register_car/" +
          car_license +
          "/" +
          status +
          "/" +
          last_update);

      showDialog(
          context: context,
          builder: (context) {
            if (response.statusCode == 200) {
              return AlertDialog(
                  title: Text("Sucess"),
                  actions: [
                    TextButton(
                        onPressed: () => Navigator.of(context).pop(),
                        child: Text("Ok"))
                  ],
                  content: StatefulBuilder(
                    builder: (context, setstate) {
                      return SingleChildScrollView(
                        child: ListBody(
                          children: [
                            Text("Submitted successfully"),
                            Builder(builder: (context) {
                              if (pressed == true) {
                                return Text("Status code: " +
                                    response.statusCode.toString());
                              } else {
                                return OutlineButton(
                                  onPressed: () {
                                    setstate(() {
                                      pressed = true;
                                    });
                                  },
                                  child: Text("Show more information"),
                                );
                              }
                            })
                          ],
                        ),
                      );
                    },
                  ));
            } else {
              return AlertDialog(
                  title: Text("Fail"),
                  actions: [
                    TextButton(
                        onPressed: () => Navigator.of(context).pop(),
                        child: Text("Ok"))
                  ],
                  content: StatefulBuilder(
                    builder: (context, setstate) {
                      return SingleChildScrollView(
                        child: ListBody(
                          children: [
                            Text("Fail to submit"),
                            Builder(builder: (context) {
                              if (pressed == true) {
                                return Text("Status code: " +
                                    response.statusCode.toString());
                              } else {
                                return OutlineButton(
                                  onPressed: () {
                                    setstate(() {
                                      pressed = true;
                                    });
                                  },
                                  child: Text("Show more information"),
                                );
                              }
                            })
                          ],
                        ),
                      );
                    },
                  ));
            }
          });
      //sent");
      //print(response);
    }

    Future show_dialog_2(context) async {
      final return_dialog = await showTimePicker(
          context: context, initialTime: TimeOfDay.fromDateTime(selectedDate));
      if (return_dialog != null && return_dialog != selectedDate)
        setState(() {
          seletedTime = return_dialog;
          selectedDate = DateTime(selectedDate.year, selectedDate.month,
              selectedDate.day, seletedTime.hour, seletedTime.minute);
        });
    }

    Future show_dialog(context) async {
      final return_dialog = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        lastDate: DateTime(2025),
        firstDate: DateTime(2020),
      );
      if (return_dialog != null && return_dialog != selectedDate)
        setState(() {
          selectedDate = return_dialog;
        });
    }

    screen_handler(name, BuildContext? context, {run}) {
      if (name == "0") {
        return Container(
          alignment: Alignment.center,
          color: Colors.grey[200],
          child: image,
        );
      } else if (name == "1") {
        Future getLocation() async {
          return await Location().getLocation();
        }

        var user_location = getLocation();

        List<Marker> _markers = <Marker>[
          Marker(
              markerId: MarkerId("avaliabe parking"),
              position: LatLng(22.215833, 114.215833),
              infoWindow: InfoWindow(title: "Avaliabe parking"),
              icon: parking_icon)
        ];
        user_location.then((value) {
          _markers.add(Marker(
              markerId: MarkerId("user-location"),
              position: LatLng(value.latitude, value.longitude),
              infoWindow:
                  InfoWindow(title: "Your location when you enter this page")));
        });
        return user_location.then((value) => GoogleMap(
              mapType: MapType.normal,
              initialCameraPosition: CameraPosition(
                  target: LatLng(value.latitude, value.longitude), zoom: 18),
              onMapCreated: (GoogleMapController controller) {
                //_controller.complete(controller);
              },
              markers: Set<Marker>.of(_markers),
            ));

        /*
        return AlertDialog(
          title: Text("No map avaliable"),
          content: Text("No map avaliable because we don't have API key"),
        );
  */
        /*
        return GoogleMap(
            mapType: MapType.hybrid,
            initialCameraPosition: CameraPosition(
              target: LatLng(37.42796133580664, -122.085749655962),
              zoom: 14.4746,
            ));
          */
      } else if (name == "4") {
        return ListView(children: [
          ListTile(
            title: Text("About"),
            onTap: () {
              screen = "about";
              setState(() {});
            },
          ),
          Builder(
              builder: (context) => ListTile(
                    title: Text("License"),
                    onTap: () {
                      showLicensePage(
                          context: context,
                          applicationName: "Smart Parking",
                          applicationIcon: Icon(Icons.local_parking),
                          applicationLegalese:
                              "Octopus card photo from www.octopus.com.hk");
                      //not sure do I use applicationLegalese wrong
                    },
                  )),
          ListTile(
            title: Text("Chat"),
            onTap: () {
              screen = "chatbot";
              setState(() {});
            },
          ),
          ListTile(
            title: Text("Show Chat button"),
            onTap: () {
              show_button = true;
              setState(() {});
            },
          ),
          ListTile(
            title: Text("Register Octopus card"),
            onTap: () {
              screen = "Register car";
              setState(() {});
            },
          ),
          Container(child: Image.asset("assets/image/photo2.jpeg")),
        ]);
      } else if (name == "about") {
        return SizedBox(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(
              Icons.local_parking,
              size: 100,
            ),
            Text(
              "Smart parking",
              style: new TextStyle(fontSize: 20),
            ),
            Text(
              "\n\nHong Kong didn't have enought place to place car anymore,but we want to fix it. We thought we can use puzzle to slove this. This can free up a lot of space in local parking. And use puzzle to move car in and out.",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 15,
              ),
            ),
          ],
        ));
      } else if (name == "2") {
        //print(API(http.Client()).toString());
        bool? finished;
        if (run != null) {
          finished = run;
        } else {
          finished = false;
        }
        //var API_result = API(http.Client()) as List;

        bool finish = false;
        return FutureBuilder<List>(
            future: API_(http.Client()),
            builder: (context, _list_) {
              if (_list_.hasData == true) {
                //print("has");

                var now_list = _list_.data;
                //print(now_list is List);

                var car_data_ = get_car(context);
                car_data_.then((value) {
                  if (true) {
                    //print(value);
                    //print(_list_.data!.length);
                    for (var index = 0; index <= _list_.data!.length;) {
                      //print(index);
                      try {
                        //print(now_list![index]["car_license"]);
                        //print(snapshot.data.toString());
                        //print(_list_.data![index]);
                        if (now_list![index]["car_license"].toString() ==
                            value.toString()) {
                          //print("sumit");
                          screen = "${now_list[index]}";
                          is_sent = "submited";
                          finish = true;
                          setState(() {});
                        } else {
                          if (finish == true) {
                            break;
                          } else {}
                        }
                        index = index + 1;
                      } catch (e) {
                        //print(e);
                        break;
                      }
                    }
                  }
                });

                return (Column(
                  children: [
                    SizedBox(
                      width: double.infinity,
                      child: Text(
                        "No any parking data or history",
                        style: TextStyle(
                          fontSize: 25,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    OutlineButton(
                      onPressed: () {
                        screen = "3";
                        setState(() {});
                      },
                      child: Text("Schedule my car exit"),
                    )
                  ],
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                ));
              } else {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            });
      } else if (screen.contains("{") && screen.contains("}")) {
        return ListView.builder(
          itemCount: screen.split(",").length,
          itemBuilder: (_, i) {
            if ("$i" == "0") {
              return ListTile(
                title: Text(
                    "Octopus card number: ${screen.replaceAll('{', "").replaceAll('car_license:', "").split(',')[i]}"),
              );
            } else if ("$i" == "1") {
              return ListTile(
                title: Text(
                    "Status: ${screen.replaceAll("status:", "").split(",")[i]}"),
              );
            } else if ("$i" == "2") {
              return ListTile(
                title: Text(
                    "Time: ${screen.replaceAll('}', "").replaceAll('last_update:', "").split(",")[i]}"),
              );
            }
            return Container();
          },
        );
      } else if (screen == "3") {
        redirected = true;
        return SingleChildScrollView(
          child: Center(
              child: Padding(
            padding: EdgeInsets.only(top: 10),
            child: Column(
              children: [
                OutlineButton(
                  child: Text("Set date"),
                  onPressed: () {
                    show_dialog(context);
                  },
                ),
                OutlineButton(
                  onPressed: () {
                    show_dialog_2(context);
                  },
                  child: Text(
                    "Set time",
                  ),
                ),
                Text(
                  selectedDate.year.toString() +
                      "-" +
                      selectedDate.month.toString() +
                      "-" +
                      selectedDate.day.toString() +
                      "\n" +
                      selectedDate.hour.toString().padLeft(2, '0') +
                      ":" +
                      selectedDate.minute.toString().padLeft(2, '0'),
                  style: Theme.of(context!).textTheme.bodyText1,
                ),
                OutlineButton(
                  onPressed: () {
                    DateTime time_ = DateTime.now();
                    get_car(context).then((value) => Sent_data(
                        http.Client(),
                        value,
                        "Exit",
                        selectedDate.toLocal().toString().replaceAll(" ", "-"),
                        context));

                    setState(() {});
                  },
                  child: Text("exit"),
                ),
                Container(
                  child: Image.asset("assets/image/photo.jpeg"),
                  padding: EdgeInsets.all(10),
                )
              ],
            ),
          )),
        );
      } else if (screen == "chatbot") {
        var message_;
        var now_message;
        Future.delayed(Duration(milliseconds: 500), () {
          setState(() {});
        });
        //print(message.length);
        if (message.isEmpty) {
        } else {
          now_message = message.reversed.toList()[0] as Text;
          now_message = now_message.data;
        }
        //print(now_message == "Chatbot: I will redirect you to car info page");
        if (now_message == "Chatbot: I will redirect you to car info page") {
          if (redirected == false) {
            //print("now");
            screen = "2";

            redirected = true;
            return Container(
              child: Text("Redirecting you to car info"),
            );
          } else {
            //print("else");

            return Column(
              children: [
                Expanded(
                    child: SizedBox(
                  child: ListView.builder(
                    reverse: true,
                    itemCount: message.length,
                    itemBuilder: (context, index) {
                      Widget message_ = message.reversed.toList()[index];
                      if ((index + 1).isOdd) {
                        return Bubble(
                          child: message_,
                          color: Color.fromRGBO(225, 255, 199, 1.0),
                          nip: BubbleNip.rightBottom,
                          alignment: Alignment.center,
                        );
                      } else {
                        Bubble(
                          child: message_,
                          nip: BubbleNip.leftTop,
                          alignment: Alignment.center,
                        );
                      }
                      return Container();
                    },
                  ),
                )),
                Container(
                  alignment: Alignment.bottomCenter,
                  padding: EdgeInsets.all(15),
                  child: Container(
                      child: SizedBox(
                    child: Row(
                      children: [
                        Expanded(
                            child: TextField(
                          onSubmitted: (value) {
                            message.add(Text("You: " + message_to_sent.text));
                            setState(() {});
                            Chatbot(http.Client(), message_to_sent.text).then(
                                (value) => message
                                    .add(Text("Chatbot: " + value.body)));
                            message_to_sent.text = "";
                            redirected = false;
                          },
                          textInputAction: TextInputAction.send,
                          controller: message_to_sent,
                        )),
                        FlatButton(
                          child: Icon(Icons.send),
                          onPressed: () {
                            message.add(Text("You: " + message_to_sent.text));
                            setState(() {});
                            Chatbot(http.Client(), message_to_sent.text).then(
                                (value) => message
                                    .add(Text("Chatbot: " + value.body)));
                            message_to_sent.text = "";
                            redirected = false;
                          },
                        )
                      ],
                    ),
                  )),
                )
              ],
            );
          }
        } else {
          //print("else2");
          return Column(
            children: [
              Expanded(
                  child: SizedBox(
                child: ListView.builder(
                  reverse: true,
                  itemCount: message.length,
                  itemBuilder: (context, index) {
                    Widget message_ = message.reversed.toList()[index];
                    if ((index).isOdd) {
                      //print("odd");
                      return Bubble(
                        child: message_,
                        color: Color.fromRGBO(225, 255, 199, 1.0),
                        nip: BubbleNip.rightBottom,
                        margin: BubbleEdges.only(top: 10),
                        alignment: Alignment.center,
                      );
                    } else if ((index).isEven) {
                      //print("even");
                      return Bubble(
                        margin: BubbleEdges.only(top: 10),
                        child: message_,
                        nip: BubbleNip.leftTop,
                        alignment: Alignment.center,
                      );
                    } else {
                      //print("weird");
                    }
                    return Container();
                  },
                ),
              )),
              Container(
                alignment: Alignment.bottomCenter,
                padding: EdgeInsets.all(15),
                child: Container(
                    child: SizedBox(
                  child: Row(
                    children: [
                      Expanded(
                          child: TextField(
                        onSubmitted: (value) {
                          message.add(Text("You: " + message_to_sent.text));
                          setState(() {});
                          Chatbot(http.Client(), message_to_sent.text).then(
                              (value) =>
                                  message.add(Text("Chatbot: " + value.body)));
                          message_to_sent.text = "";
                          redirected = false;
                        },
                        textInputAction: TextInputAction.send,
                        controller: message_to_sent,
                      )),
                      FlatButton(
                        child: Icon(Icons.send),
                        onPressed: () {
                          message.add(Text("You: " + message_to_sent.text));
                          setState(() {});
                          Chatbot(http.Client(), message_to_sent.text).then(
                              (value) =>
                                  message.add(Text("Chatbot: " + value.body)));
                          message_to_sent.text = "";
                          redirected = false;
                        },
                      )
                    ],
                  ),
                )),
              )
            ],
          );
        }
      } else if (screen == "Register car") {
        return SingleChildScrollView(
          child: ListBody(
            children: [
              SizedBox(
                height: 25,
              ),
              Image.asset("assets/image/octoupus_card.jpg"),
              SingleChildScrollView(
                  child: Container(
                      margin: EdgeInsets.only(
                          left: 50, right: 50, top: 85, bottom: 100),
                      child: DecoratedBox(
                        decoration: BoxDecoration(color: Colors.white),
                        child: Column(
                          children: [
                            Text(
                              "Register Octopus card",
                              style: TextStyle(
                                  fontSize: 25, fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              height: 50,
                            ),
                            Container(
                                child: TextField(
                              decoration: InputDecoration(
                                border: OutlineInputBorder(),
                                hintText: "octopus card number",
                              ),
                              controller: car_textbox,
                            )),
                            OutlineButton(
                              onPressed: () {
                                nfc(context);
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text("Detect Octoupus card "),
                                  Icon(Icons.nfc)
                                ],
                              ),
                            ),
                            Center(
                              child: OutlineButton(
                                child: Text("register"),
                                onPressed: () {
                                  save_car(car_textbox.text);
                                  screen = "0";
                                  setState(() {});
                                },
                              ),
                            ),
                          ],
                          mainAxisAlignment: MainAxisAlignment.center,
                        ),
                      )))
            ],
          ),
        );
      }
    }

    //print(MaterialLocalizations.of(context));
    return MaterialApp(
      title: title,
      home: Scaffold(
        appBar: AppBar(
          title: Text(title),
          backgroundColor: Colors.blue,
        ),
        floatingActionButton: show_button
            ? Padding(
                padding: EdgeInsets.only(bottom: 100),
                child: FloatingActionButton(
                  onPressed: () {
                    screen = "chatbot";

                    setState(() {});
                    Icon(Icons.chat);
                  },
                  child: GestureDetector(
                    child: Icon(Icons.chat),
                    onLongPress: () {
                      show_button = false;
                      setState(() {});
                    },
                  ),
                ),
              )
            : Container(),
        body: Builder(
          builder: (context) {
            final return_ = screen_handler(screen, context);
            if (return_ is Future) {
              return FutureBuilder(
                future: screen_handler(screen, context) as Future,
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return Center(child: CircularProgressIndicator());
                  } else if (snapshot.connectionState ==
                      ConnectionState.waiting) {
                    return Center(child: CircularProgressIndicator());
                  } else {
                    //print(snapshot.data);
                    return Container(child: snapshot.data as Widget);
                  }
                },
              );
            } else if (return_ is Widget) {
              return return_;
            }
            return Container();
          },
        ), //screen_handler(screen, context),
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          currentIndex: index_now,
          selectedItemColor: Colors.amber[800],
          onTap: (int index) {
            is_sent = "You haven't submit yet";
            setState(() {
              index_now = index;
              screen = index.toString();
            });
          },
          items: [
            BottomNavigationBarItem(label: "Home", icon: Icon(Icons.home)),
            BottomNavigationBarItem(label: "Map", icon: Icon(Icons.map)),
            BottomNavigationBarItem(
                label: "Car info", icon: Icon(Icons.directions_car)),
            BottomNavigationBarItem(
                label: "Car exit", icon: Icon(Icons.assignment_turned_in)),
            BottomNavigationBarItem(
                label: "Setting", icon: Icon(Icons.settings)),
          ],
        ),
      ),
    );
  }
}
